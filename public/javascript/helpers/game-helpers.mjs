import { appendRoomElement } from '../views/room.mjs';

export const displayRooms = (rooms, roomsContainer, socket) => {
    roomsContainer.innerHTML = '';
    rooms.forEach(room => {
        appendRoomElement({
            name: room.name,
            numberOfUsers: room.users.length,
            onJoin: () => {
                socket.emit('JOIN_ROOM', room.name);
            }
        });
    });
};

export const fetchRandomText = (id) => {
    return fetch(`/game/texts/${id}`)
        .then(response => response.text());
};


export const updateText = (text, currentIndex, textContainer) => {
    const highlightedText = text.slice(0, currentIndex);
    const nextChar = currentIndex < text.length ? text[currentIndex] : '';
    const remainingText = currentIndex < text.length - 1 ? text.slice(currentIndex + 1) : '';

    textContainer.innerHTML = `
      <span class="highlight">${highlightedText}</span>
      <span class="underline">${nextChar === ' ' ? '&nbsp;' : nextChar}</span>${remainingText}`;
};

export const updateProgress = (username, correctCharacters, totalCharacters, socket) => {
    const progress = Math.floor((correctCharacters / totalCharacters) * 100);
    socket.emit('UPDATE_PROGRESS', progress);
};
