import { showInputModal, showMessageModal, showResultsModal } from './views/modal.mjs';
import { addClass, removeClass } from './helpers/dom-helper.mjs';
import { appendUserElement, changeReadyStatus, setProgress } from './views/user.mjs';
import { displayRooms, fetchRandomText, updateProgress, updateText } from './helpers/game-helpers.mjs';

const username = sessionStorage.getItem('username');

if (!username) {
    window.location.replace('/login');
}

const socket = io('', { query: { username } });

let timerInterval;
let handleKeyPress;

const createRoomButton = document.getElementById('add-room-btn');
const roomsContainer = document.querySelector('#rooms-wrapper');
const roomsPage = document.getElementById('rooms-page');

const gamePage = document.getElementById('game-page');
const roomNameElement = document.getElementById('room-name');
const readyButton = document.getElementById('ready-btn');
const quitRoomButton = document.getElementById('quit-room-btn');
const usersContainer = document.querySelector('#users-wrapper');


const timer = document.getElementById('timer');
const textContainer = document.getElementById('text-container');
const gameTimer = document.getElementById('game-timer');
const gameTimerSeconds = document.getElementById('game-timer-seconds');


createRoomButton.addEventListener('click', () => {
    let roomName = '';
    showInputModal({
        title: 'Create room',
        onChange: value => {
            roomName = value;
        },
        onSubmit: () => {
            socket.emit('CREATE_ROOM', roomName);
        }
    });
});

readyButton.addEventListener('click', () => {
    socket.emit('TOGGLE_READY');
});

quitRoomButton.addEventListener('click', () => {
    socket.emit('QUIT_ROOM');
    addClass(gamePage, 'display-none');
    removeClass(roomsPage, 'display-none');
});

socket.on('USER_EXISTS', () => {
    sessionStorage.removeItem('username');
    showMessageModal({
        message: 'User already exists',
        onClose: () => window.location.replace('/login')
    });
});

socket.on('ROOM_DETAILS', ({ name, users }) => {
    addClass(roomsPage, 'display-none');
    removeClass(gamePage, 'display-none');
    roomNameElement.innerText = name;

    usersContainer.innerHTML = '';
    for (const user of users) {
        appendUserElement({
            username: user.username,
            ready: user.ready,
            isCurrentUser: user.username === username
        });
    }
});

socket.on('INVALID_ROOM_NAME', () => {
    showMessageModal({
        message: 'The room with this name already exists'
    });
});

socket.on('CHANGE_READY', ({ username, ready }) => {
    changeReadyStatus({ username, ready });
    if (username === sessionStorage.getItem('username'))
        readyButton.innerText = ready ? 'Not ready' : 'Ready';
});

socket.on('START_TIMER', ({ countdown }) => {
    addClass(readyButton, 'display-none');
    addClass(quitRoomButton, 'display-none');

    removeClass(timer, 'display-none');
    timer.innerText = countdown;
});

socket.on('UPDATE_TIMER', ({ countdown }) => {
    timer.innerText = countdown;
});

socket.on('START_GAME', async ({ users, raceTextId, gameCountdown }) => {
    addClass(timer, 'display-none');
    removeClass(textContainer, 'display-none');

    const text = await fetchRandomText(raceTextId);
    const user = users.find(user => user.username === username).username;

    let currentIndex = 0;
    const totalCharacters = text.length;
    let correctCharacters = 0;

    removeClass(gameTimer, 'display-none');
    gameTimerSeconds.innerText = gameCountdown;

    timerInterval = setInterval(() => {
        gameCountdown--;
        socket.emit('UPDATE_GAME_TIMER', gameCountdown);
        gameTimerSeconds.innerText = gameCountdown;
        if (gameCountdown === 0) {
            clearInterval(timerInterval);
            socket.emit('TIMER_OVER');
        }
    }, 1000);


    updateText(text, currentIndex, textContainer);
    updateProgress(user, correctCharacters, totalCharacters, socket);

    handleKeyPress = (event) => {
        const enteredCharacter = event.key;

        if (enteredCharacter === text[currentIndex]) {
            currentIndex++;
            correctCharacters++;
            updateText(text, currentIndex, textContainer);
            updateProgress(user, correctCharacters, totalCharacters, socket);
        }
    };

    document.addEventListener('keydown', handleKeyPress);
});

socket.on('UPDATED_PROGRESS', ({ username, progress }) => {
    setProgress({ username, progress });
});

socket.on('GAME_OVER', ({ users }) => {
    const usersSortedArray = users.sort((a, b) => {
        if (b.progress !== a.progress) {
            return b.progress - a.progress;
        } else {
            return a.time - b.time;
        }
    }).map(user => user.username);

    clearInterval(timerInterval);
    addClass(gameTimer, 'display-none');
    document.removeEventListener('keydown', handleKeyPress);

    showResultsModal({
        usersSortedArray, onClose: () => {
            removeClass(readyButton, 'display-none');
            removeClass(quitRoomButton, 'display-none');
            addClass(textContainer, 'display-none');
        }
    });
});

socket.on('NEW_GAME', ({ users }) => {
    users.forEach(({ username, ready, progress }) => {
        setProgress({ username, progress });
        changeReadyStatus({ username, ready });
        if (username === sessionStorage.getItem('username'))
            readyButton.innerText = ready ? 'Not ready' : 'Ready';
    });
});
socket.on('ROOMS_LIST', (rooms) => displayRooms(rooms, roomsContainer, socket));
socket.emit('GET_ROOMS');
