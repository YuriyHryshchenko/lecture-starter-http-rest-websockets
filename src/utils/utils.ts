import { MAXIMUM_USERS_FOR_ONE_ROOM, SECONDS_TIMER_BEFORE_START_GAME } from '../socket/config.js';
import { Server, Socket } from 'socket.io';
import type { Room } from '../types/types.js';
import {
    areAllUsersReady,
    findRoomByUsername,
    getAvailableRooms,
    getRandomTextId,
    sendRoomDetails
} from '../helpers/helpers.js';

export const joinRoom = (rooms: Room[], io: Server, socket: Socket, username: string, roomName: string) => {
    const room = rooms.find(room => room.name === roomName);

    if (!room || room.users.length === MAXIMUM_USERS_FOR_ONE_ROOM) {
        return;
    }

    room.users.push({
        username,
        ready: false,
        progress: 0,
        time: 0
    });
    socket.join(roomName);

    sendRoomDetails(io, room, 'ROOM_DETAILS');
    io.emit('ROOMS_LIST', getAvailableRooms(rooms));
};

export const removeUserFromRoom = (rooms: Room[], io: Server, username: string) => {
    const room = findRoomByUsername(rooms, username);

    if (!room) {
        return;
    }

    room.users.splice(
        room.users.findIndex(item => item.username === username),
        1
    );

    if (room.users.length === 0) {
        rooms.splice(rooms.indexOf(room), 1);
    }

    sendRoomDetails(io, room, 'ROOM_DETAILS');

    if (areAllUsersReady(room) && !room.isGameStarted) {
        room.isGameStarted = true;
        startTimer(io, room);
    }

    if (room.users.every(user => user.progress === 100)) {
        gameOver(io, room, rooms);
    }
    io.emit('ROOMS_LIST', getAvailableRooms(rooms));
};

export const startTimer = (io: Server, room: Room) => {
    room.countdown = SECONDS_TIMER_BEFORE_START_GAME;
    sendRoomDetails(io, room, 'START_TIMER');
    const timerInterval = setInterval(() => {
        if (room.countdown) {
            room.countdown--;
            sendRoomDetails(io, room, 'UPDATE_TIMER');
            if (room.countdown === 0) {
                clearInterval(timerInterval);
                room.raceTextId = getRandomTextId();
                sendRoomDetails(io, room, 'START_GAME');
            }
        }
    }, 1000);
};

export const gameOver = (io: Server, room: Room, rooms: Room[]) => {
    sendRoomDetails(io, room, 'GAME_OVER');
    room.isGameStarted = false;
    room.countdown = null;
    room.users.forEach(user => {
        user.progress = 0;
        user.ready = false;
        user.time = 0;
    });
    io.emit('ROOMS_LIST', getAvailableRooms(rooms));
    sendRoomDetails(io, room, 'NEW_GAME');
};
