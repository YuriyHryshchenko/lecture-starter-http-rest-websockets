export interface Room {
    name: string;
    users: User[];
    isGameStarted: boolean;
    raceTextId: number | null;
    countdown: number | null;
    gameCountdown: number;
}

export interface User {
    username: string;
    ready: boolean;
    progress: number;
    time: number;
}
