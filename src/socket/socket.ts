import { Server } from 'socket.io';
import type { Room, User } from '../types/types.js';
import { gameOver, joinRoom, removeUserFromRoom, startTimer } from '../utils/utils.js';
import { SECONDS_FOR_GAME } from './config.js';
import {
    areAllUsersReady,
    findRoomByUsername,
    findUserInRoom,
    getAvailableRooms,
    isValidRoomName
} from '../helpers/helpers.js';

const users: User[] = [];
const rooms: Room[] = [];
export default (io: Server) => {
    io.on('connection', socket => {
        const username = socket.handshake.query.username;

        if (users.some(user => user.username === username)) {
            socket.emit('USER_EXISTS', username);
            socket.disconnect();
            return;
        }
        users.push({
            username: username as string,
            ready: false,
            progress: 0,
            time: 0
        });

        socket.on('GET_ROOMS', () => {
            const availableRooms = getAvailableRooms(rooms);
            socket.emit('ROOMS_LIST', availableRooms);
        });

        socket.on('CREATE_ROOM', (roomName: string) => {
            if (!isValidRoomName(rooms, roomName)) {
                socket.emit('INVALID_ROOM_NAME', roomName);
                return;
            }

            const newRoom: Room = {
                name: roomName,
                users: [],
                countdown: null,
                isGameStarted: false,
                raceTextId: null,
                gameCountdown: SECONDS_FOR_GAME
            };
            rooms.push(newRoom);
            joinRoom(rooms, io, socket, username as string, roomName);
        });

        socket.on('JOIN_ROOM', (roomName: string) => {
            joinRoom(rooms, io, socket, username as string, roomName);
        });

        socket.on('QUIT_ROOM', () => {
            const room = findRoomByUsername(rooms, username as string);
            if (!room) {
                return;
            }
            socket.leave(room.name);
            removeUserFromRoom(rooms, io, username as string);
        });

        socket.on('TOGGLE_READY', () => {
            const room = findRoomByUsername(rooms, username as string);

            if (!room) {
                return;
            }

            const roomUser = findUserInRoom(room, username as string);

            if (roomUser) {
                roomUser.ready = !roomUser.ready;
                io.to(room.name).emit('CHANGE_READY', roomUser);

                if (areAllUsersReady(room) && !room.isGameStarted) {
                    room.isGameStarted = true;
                    io.emit('ROOMS_LIST', getAvailableRooms(rooms));
                    startTimer(io, room);
                }
            }
        });

        socket.on('UPDATE_PROGRESS', (progress: number) => {
            const room = findRoomByUsername(rooms, username as string);
            if (!room) {
                return;
            }

            const roomUser = findUserInRoom(room, username as string);
            if (roomUser) {
                roomUser.progress = progress;
                io.to(room.name).emit('UPDATED_PROGRESS', roomUser);
            }

            if (room.users.every(user => user.progress === 100)) {
                gameOver(io, room, rooms);
            }
        });

        socket.on('UPDATE_GAME_TIMER', (gameCountdown: number) => {
            const room = findRoomByUsername(rooms, username as string);
            if (!room) {
                return;
            }

            const user = findUserInRoom(room, username as string);
            if (user && user.progress !== 100) {
                user.time = SECONDS_FOR_GAME - gameCountdown;
            }
        });

        socket.on('TIMER_OVER', () => {
            const room = findRoomByUsername(rooms, username as string);
            if (!room) {
                return;
            }
            gameOver(io, room, rooms);
        });

        socket.on('disconnect', () => {
            users.splice(
                users.findIndex(item => item.username === username),
                1
            );
            removeUserFromRoom(rooms, io, username as string);
        });
    });
};
