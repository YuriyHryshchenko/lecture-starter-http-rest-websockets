import path from 'node:path';
import { Router } from 'express';

import { HTML_FILES_PATH } from '../config.js';
import { texts } from '../data.js';

const router = Router();

router.get('/', (req, res) => {
    const page = path.join(HTML_FILES_PATH, 'game.html');
    res.sendFile(page);
});

router.get('/texts/:id', (req, res) => {
    const textId = req.params.id;
    const text = texts[textId];

    if (text) {
        res.status(200).send(text);
    } else {
        res.status(404).send('Text not found');
    }
});

export default router;
