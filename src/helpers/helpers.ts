import type { Room } from '../types/types.js';
import { MAXIMUM_USERS_FOR_ONE_ROOM } from '../socket/config.js';
import { texts } from '../data.js';
import { Server } from 'socket.io';

export const getAvailableRooms = (rooms: Room[]) =>
    rooms.filter(room => room.users.length < MAXIMUM_USERS_FOR_ONE_ROOM && !room.isGameStarted);

export const isValidRoomName = (rooms: Room[], roomName: string) =>
    roomName && !rooms.some(room => room.name === roomName);

export const findRoomByUsername = (rooms: Room[], username: string) =>
    rooms.find(room => room.users.some(user => user.username === username));

export const findUserInRoom = (room: Room, username: string) => room.users.find(user => user.username === username);

export const areAllUsersReady = (room: Room) => room.users.every(user => user.ready);

export const getRandomTextId = () => Math.floor(Math.random() * texts.length);
export const sendRoomDetails = (io: Server, room: Room, event: string) => {
    io.to(room.name).emit(event, room);
};
